# MSAuthentication-iOS

![iOS 11.0+](https://img.shields.io/badge/iOS-11.0%2B-blue.svg)
![Swift 5.0+](https://img.shields.io/badge/Swift-5.0%2B-orange.svg)

### Presentation

**MSAuthentication** is a library for authenticating...


## Usage

Checkout the example project **MSAuthenticationExampleApp**

### Initiliaze the framework

```swift
MSAuthClient.shared.initialize(clientId: "your-client-id", clientSecret: "your-client-secret")
```


### List of available routes

```swift
// Creates the user account
createAccount(email: String, password: String, phoneNumber: String?, completion: @escaping (_ message: String?,_ error: String?) -> Void)

// Authenticates the user with his phone number
authenticate(phoneNumber: String, completion: @escaping (_ token: MSToken?, _ error: String?) -> Void)

// Authenticates the user with his email and password
authenticate(email: String, password: String, completion: @escaping (_ token: MSToken?, _ error: String?) -> Void)

// Retrieves a token
authorize(voucher: String, otp: String, completion: @escaping (_ token: MSToken?, _ error: String?) -> Void)

// Retrieves a new token from the refresh token
renewToken(refreshToken: String, completion: @escaping (_ token: MSToken?, _ error: String?) -> Void)

// Activates the two factor authentication
activateTwoFactor(accessToken: String, phoneNumber: String, completion: @escaping (_ error: String?) -> Void)

// Deactivates the two factor authentication
deactivateTwoFactor(accessToken: String, completion: @escaping (_ error: String?) -> Void)

// Sends a message to a phone number 
sendMessage(phoneNumber: String, message: String, completion: @escaping (_ response: String?, _ error: String?) -> Void)
```

### Example creating an account

```swift
MSAuthClient.shared.createAccount(email: "user-email-address", password: "user-password") { message, error in
	if let message = message {
		print(message)
	}
	if let error = error {
		print(error)
	}
}
```

## Installation

### CocoaPods

Add the following entry to your Podfile:

```rb
pod 'MSAuthentication'
```

Then run `pod install`.

Don't forget to `import MSAuthentication` in every file you'd like to use **MSAuthentication**.


### Swift Package Manager (Swift 5.3 required)

To integrate using Apple's Swift package manager, add the following as a dependency to your Package.swift:

```swift
.package(url: "https://gitlab.com/madseven-public/msauthentication-ios.git", .upToNextMajor(from: "1.0.0"))
```

and then specify `"MSAuthentication"` as a dependency of the Target in which you wish to use MSAuthentication.

### Manually

1. Drag and drop MSAuthentication.xcframework to your Xcode project. When prompted “Choose options for adding these files:” make sure **Copy items if needed** and your project target is selected. 

2. We need now to set new paths in the **Import Paths** of the target's build settings. Providing the path to the directory where each and every `.swiftmodule` directories are will allow your application to interract with the static library.
Add the following paths:
* `$(PROJECT_DIR)/path-to-framework/MSAuthentication.xcframework/ios-arm64`
* `$(PROJECT_DIR)/path-to-framework/MSAuthentication.xcframework/ios-x86_64-simulator`
