//
//  ViewController.swift
//  MSAuthenticationExample
//
//  Created by Morgan Le Gal on 13/07/2020.
//  Copyright © 2020 madseven. All rights reserved.
//

import MSAuthentication
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize the SDK with your API Key and Secret
        MSAuthClient.shared.initialize(clientId: "your-client-id", clientSecret: "your-client-secret")
        
        // Creates the user account
        MSAuthClient.shared.createAccount(email: "your-phone-number", password: "your-phone-number", phoneNumber: "your-phone-number") { message, error in
            if let message = message {
                print("Message: \(message)")
            } else if let error = error {
                print("Error: \(error)")
            }
        }

        // Authenticates the user with his phone number
        MSAuthClient.shared.authenticate(phoneNumber: "your-phone-number") { token, error in
            if let token = token {
                print("AccessToken: \(token.accessToken ?? "nil")\nRefreshToken: \(token.refreshToken ?? "nil")\nExpires in: \(token.expiresIn)")
            } else if let error = error {
                print("Error: \(error)")
            }
        }

        // Authenticates the user with his email and password
        MSAuthClient.shared.authenticate(email: "your-email", password: "your-password") { token, error in
                   if let token = token {
                       print("AccessToken: \(token.accessToken ?? "nil")\nRefreshToken: \(token.refreshToken ?? "nil")\nExpires in: \(token.expiresIn)")
                   } else if let error = error {
                       print("Error: \(error)")
                   }
               }

        // Retrieves a token
        MSAuthClient.shared.authorize(voucher: "your-voucher", otp: "your-otp") { token, error in
            if let token = token {
                print("AccessToken: \(token.accessToken ?? "nil")\nRefreshToken: \(token.refreshToken ?? "nil")\nExpires in: \(token.expiresIn)")
            } else if let error = error {
                print("Error: \(error)")
            }
        }

        // Retrieves a new token from the refresh token
        MSAuthClient.shared.renewToken(refreshToken: "your-refresh-token") { token, error in
                   if let token = token {
                       print("AccessToken: \(token.accessToken ?? "nil")\nRefreshToken: \(token.refreshToken ?? "nil")\nExpires in: \(token.expiresIn)")
                   } else if let error = error {
                       print("Error: \(error)")
                   }
               }

        // Activates the two factor authentication
        MSAuthClient.shared.activateTwoFactor(accessToken: "your-access-token", phoneNumber: "your-phone-number") { error in
           if let error = error {
               print("Error: \(error)")
           }
       }
        
        // Deactivates the two factor authentication
        MSAuthClient.shared.deactivateTwoFactor(accessToken: "your-access-token") { error in
            if let error = error {
                print("Error: \(error)")
            }
        }

        // Sends a message to a phone number
        MSAuthClient.shared.sendMessage(phoneNumber: "a-phone-number", message: "This is a Test message.") { message, error in
            if let message = message {
                print("Message: \(message)")
            } else if let error = error {
                print("Error: \(error)")
            }
        }
        
    }

}

