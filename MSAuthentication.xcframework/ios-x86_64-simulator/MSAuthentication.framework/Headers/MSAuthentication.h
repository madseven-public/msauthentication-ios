//
//  MSAuthentication.h
//  MSAuthentication
//
//  Created by Morgan Le Gal on 13/07/2020.
//  Copyright © 2020 madseven. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MSAuthentication.
FOUNDATION_EXPORT double MSAuthenticationVersionNumber;

//! Project version string for MSAuthentication.
FOUNDATION_EXPORT const unsigned char MSAuthenticationVersionString[];

#import <CommonCrypto/CommonCrypto.h>

