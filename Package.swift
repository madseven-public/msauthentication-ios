// swift-tools-version:5.3
import PackageDescription
let package = Package(
    name: "MSAuthentication",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "MSAuthentication",
            targets: ["MSAuthentication"])
    ],
    targets: [
        .binaryTarget(
            name: "MSAuthentication",
            path: "MSAuthentication.xcframework")
    ])
