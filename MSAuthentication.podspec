Pod::Spec.new do |s|
  s.name        = "MSAuthentication"
  s.version     = "1.0.0"
  s.summary     = "MadSeven's Authentication framework"
  s.description = "This framework will allow you to authenticate with 2 factor authentication a user and send text messages"
  s.homepage    = "https://gitlab.com/madseven-public/msauthentication-ios"
  s.license     = { :type => 'Apache-2.0', :file => 'LICENSE' }
  s.authors     = "MadSeven"

  s.platform = :ios, "11.0"
  s.swift_version = "5.0"
  
  s.source   = { :git => "https://gitlab.com/madseven-public/msauthentication-ios.git", :tag => s.version }
  s.vendored_frameworks   = 'MSAuthentication.xcframework'


end